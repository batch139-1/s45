import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';

const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [cp, setCp] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(() => {
        if (email != '' && password != '' && cp != '' && password == cp) {
            setIsDisabled(false);
        }
    }, [email, password, cp]);

    const registerUser = (e) => {
        e.preventDefault();
        setEmail('');
        setPassword('');
        setCp('');

        alert('Successful Registration');
    };

    return (
        <Container className="my-5">
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Form
                        className="border p-3"
                        onSubmit={(e) => registerUser(e)}
                    >
                        <h2 className="mb-3">Register</h2>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => {
                                    setEmail(e.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                        >
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(e) => {
                                    setPassword(e.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                        >
                            <Form.Control
                                type="password"
                                placeholder="Confirm Password"
                                value={cp}
                                onChange={(e) => {
                                    setCp(e.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3"
                            controlId="formBasicCheckbox"
                        >
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={registerUser}
                            disabled={isDisabled}
                        >
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default Register;
