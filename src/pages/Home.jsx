import React from 'react';
import AppNavbar from '../components/AppNavbar';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Container } from 'react-bootstrap';
import Courses from './Courses';

const Home = () => {
    return (
        <>
            <AppNavbar />
            <Container>
                <Banner />
                <Highlights />
                <Courses />
            </Container>
        </>
    );
};

export default Home;
