import React, { useEffect, useState } from 'react';
import { Card, Button, Row, Col, Container } from 'react-bootstrap';
import PropTypes from 'prop-types';

const CourseCard = ({ courseProp }) => {
    const { name, description, price } = courseProp;
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(10);
    const [isDisabled, setIsDisabled] = useState(false);
    const enroll = () => {
        if (seats !== 0) {
            setCount(count + 1);
            setSeats(seats - 1);
        } else {
            alert('No more seats left.');
        }
    };

    useEffect(() => {
        if (seats === 0) {
            setIsDisabled(true);
        }
    }, [seats]);

    return (
        <Container fluid className="mt-4">
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Card className="cardHighlights">
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Text>
                                Description: <br />
                                {description}
                            </Card.Text>
                            <Card.Text>
                                Price: <br />
                                Php {price}
                            </Card.Text>
                            <Card.Text>Enrollees: {count}</Card.Text>
                            <Card.Text>Seats left: {seats}</Card.Text>
                            <Button
                                className="btn btn-primary"
                                onClick={enroll}
                                disabled={isDisabled}
                            >
                                Enroll
                            </Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};

CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    }),
};

export default CourseCard;
