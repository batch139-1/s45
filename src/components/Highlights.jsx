import React from 'react';
import { Card, Container, Row, Col } from 'react-bootstrap';
const Highlights = () => {
    return (
        <Container className="mt-2">
            <Row className="mt-4">
                <Col xs={12} md={4}>
                    <Card className="cardHighlights">
                        <Card.Body>
                            <Card.Title>Learn from Home</Card.Title>
                            <Card.Text>
                                Lorem ipsum, dolor sit amet consectetur
                                adipisicing elit. Reiciendis ducimus veniam
                                quaerat nulla, neque maxime quae voluptates est
                                non accusantium facilis impedit soluta sed
                                tempora cum odio aliquid distinctio omnis?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="cardHighlights">
                        <Card.Body>
                            <Card.Title>Study Now, Pay Later</Card.Title>
                            <Card.Text>
                                Lorem ipsum, dolor sit amet consectetur
                                adipisicing elit. Reiciendis ducimus veniam
                                quaerat nulla, neque maxime quae voluptates est
                                non accusantium facilis impedit soluta sed
                                tempora cum odio aliquid distinctio omnis?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="cardHighlights">
                        <Card.Body>
                            <Card.Title>Learn from Home</Card.Title>
                            <Card.Text>
                                Lorem ipsum, dolor sit amet consectetur
                                adipisicing elit. Reiciendis ducimus veniam
                                quaerat nulla, neque maxime quae voluptates est
                                non accusantium facilis impedit soluta sed
                                tempora cum odio aliquid distinctio omnis?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};

export default Highlights;
