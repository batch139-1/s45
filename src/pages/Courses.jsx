import React from 'react';
import CourseCard from '../components/CourseCard';
import courseData from '../data/courseData';

const Courses = () => {
    const courses = courseData.map((course) => {
        return <CourseCard key={course.id} courseProp={course} />;
    });
    return <>{courses}</>;
};

export default Courses;
